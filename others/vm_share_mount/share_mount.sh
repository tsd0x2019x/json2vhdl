#!/bin/sh

URORDNER=mnt
SHARENAME=share # This is the name being used for shared folder between host and guest system.
# Make sure that you have installed GuestExtension already.
ORDNER=/$URORDNER/$SHARENAME
SYMLNK=$HOME/Schreibtisch/$SHARENAME

# if directory $ORDNER does not exist, create it.
if [ ! -d "$ORDNER" ]; then
  sudo mkdir $ORDNER
fi

# Assign every user/group full rights.
sudo chmod 777 $ORDNER

#sudo mount -t vboxsf -o uid=$UID,gid=$(id -g) $SHARENAME $ORDNER
sudo mount -t vboxsf -o uid=1000,gid=1000 $SHARENAME $ORDNER

# if symlink of $ORDNER exists
if [ -d $SYMLNK ]; then
  unlink $SYMLNK
fi

# create new symlink
ln -s $ORDNER $SYMLNK

