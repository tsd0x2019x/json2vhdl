/*
 * example.c
 */
#include <stdio.h>
#include <string.h>
#include "listx.h"

int main (int argc, char * argv[]) {

    /* Create new list */
    list_t * list = Init_List();

    printf("List size: %u\n", list->size);
    printf("Address (Out): %p\n", (void *)&list);

    /* Clear list */
    Clear_List2(list);

    printf("List == NULL : %s\n", (list == NULL) ? "true" : "false");
    printf("List size: %u\n", list->size);

    free(list);
    list = NULL;

    return 0;
}