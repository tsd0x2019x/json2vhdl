/*
 * listx.h
 */
#ifndef LISTX_H
#define LISTX_H

#define LISTX_API extern
#include <stdlib.h>

typedef struct Node node_t; // forward declaration of "node_t" as "struct Node"
struct Node {
    int data;
    node_t * next; // valid, since forward declaration is already made
};

typedef struct List {
    unsigned int size;
    node_t * begin;
    node_t * end;
} list_t;

/*
 * Clear all list elements and itself.
 */
LISTX_API void Clear_List (list_t ** list) {
    if ((*list) != NULL) {
        if ((*list)->size != 0) {
            node_t * pNode = NULL;
            while ((pNode = (*list)->begin) != NULL) { // free all allocated memory for [pData]
                (*list)->begin = pNode->next;
                free(pNode);
            }
            pNode = NULL;
            (*list)->end = (*list)->begin;
        }
        (*list)->size = 0;
        free(*list);
    }
    (*list) = NULL;
}

LISTX_API void Clear_List2 (list_t * list) {
    printf("Address (In): %p\n", (void *)&list);
    printf("Address (In): %p\n", list);
    free(list);
    //list->size = 2;
    list = NULL;
}

/*
 * Create a new empty list and returns the pointer to it.
 * Note: Do not forget to clear every created list after use.
 */
LISTX_API list_t * Init_List () {
    list_t * list = (list_t *) malloc(sizeof(list_t));
    list->size = 0;
    list->begin = NULL;
    list->end = NULL;
    return list;
}

/*
 * Add a new element (with data) at list end. Return new number of all list elements
 * by SUCCESS. Otherwise, return zero (0) if FAILURE.
 */
LISTX_API unsigned int AddElement(list_t * list, int data) {
    if (list == NULL) {
        fprintf(stderr, "Warning: There is no list. Could not add new element.\n");
        return 0;
    }
    if (list->size == 0) { // if empty list
        list->begin = (node_t *) malloc(sizeof(node_t));
        (list->begin)->data = data;
        (list->begin)->next = NULL;
        list->end = list->begin;
    }
    else {
        (list->end)->next = (node_t *) malloc(sizeof(node_t));
        ((list->end)->next)->data = data;
        ((list->end)->next)->next = NULL;
        list->end = (list->end)->next;
    }
    (list->size)++;
    return list->size;
}

#endif /* LISTX_H */
