/*
 * stringx.h
 */

#ifndef STRINGX_H
#define STRINGX_H

#define STRINGX_API extern

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
//#include <regex.h>

STRINGX_API void Replace_Char_In_String (char * __restrict__ string, char oldChar, char newChar);
STRINGX_API size_t Trim_String (const char * string, char * output, size_t len);
STRINGX_API char * Str_Concat (char * dest, size_t max_size, int num, ...);
STRINGX_API char * Str_InsertFirst (char * src, char * dest, int dest_max_size);
STRINGX_API char * Duplicate_String (const char * __restrict__ string);
STRINGX_API char * Duplicate_String_N (const char * __restrict__ string, size_t N);

//=============================================================================================================================
/*
 * Checks whether a string matches a pattern (POSIX regular expression).
 * 
 * @return:
 *      Zero (0):    Success (Match)
 *      No-Zero (1): No match
 */
/*
STRINGX_API int Regex_Match (char * pattern, char * string) {
    regex_t regex;
    int reti, status;
    const int buff_size = 100;
    char errbuff[buff_size];

    // 1. Prepare and compile regular expression
    reti = regcomp(&regex, pattern, REG_EXTENDED); // returns zero for a successful compilation or an error code for failure.

    if ( reti ) {
        fprintf(stderr, "Error (Regex_Match): Could not compile regex.\n");
        exit(EXIT_FAILURE);
    }

    // 2. Execute regular expression
    reti = regexec(&regex, string, 0, NULL, 0); // returns zero for a successful match or REG_NOMATCH for failure.

    if ( !reti ) { // Match
        status = 0;
    }
    else if ( reti == REG_NOMATCH ) {
        status = 1; // No match
    }
    else {
        // Raise error
        regerror(reti, &regex, errbuff, buff_size); // buff_size == sizeof(errbuff)
        fprintf(stderr, "Error (Regex_Match): Could not execute regex => %s\n", errbuff);
        exit(EXIT_FAILURE);
    }
    // 3. Free compiled regular expression for next use of regex_t
    regfree(&regex);

    return status;
}
*/

/*
 * Replace all occurences of character [oldChar] in string [string] by new character [newChar].
 * @params
 *      [string] : String all occurences of character [oldChar] within which will be replaced by [newChar]
 *      [oldChar] : Character(s) that will be replaced all by [newChar]
 *      [newChar] : Character(s) that will replace the old [oldChar]
 */ 
STRINGX_API void Replace_Char_In_String (char * __restrict__ string, char oldChar, char newChar) {
    char * c = string;
    while (*c) {
        if (*c == oldChar) { *c = newChar; }        
        c++;
    }
}

/*
 * Trim all white-space characters from [string]. The trimmed string is then stored in [output]. 
 * @params
 *      [string] : The source string that will be trimmed.
 *      [output] : This string represents the place where the trimmed string is stored to.
 *      [len] : Size of the string [output] including the null-terminated character (\0).
 */
STRINGX_API size_t Trim_String (const char * string, char * output, size_t len)  {

    if (len == 0) { return 0; }

    /* Trim all leading spaces */
    while(isspace((unsigned char) *string)) { string++; }

    if ( *string == 0 ) {
        *output = 0;
        return 1;
    }

    /* Trim all trailing spaces */
    const char * end = string + strlen(string) - 1;
    while (end > string && isspace((unsigned char) *end)) { end--; }

    /* Determine the minimum number of (char) bytes to be copied */
    size_t num_bytes = (end - string + 1) < len ? (end - string + 1) : (len - 1);
    //memcpy(output, string, num_bytes);
    strncpy(output, string, num_bytes);

    return num_bytes;
}

/*
 * This function concatenates/combines the strings given in the va_list, which is represented by 
 * three dots (...), and then appends them to the end of string [dest].
 * 
 * @params:
 *      [dest]
 *          Pointer to the first position of the destionation string.
 *      [max_size]
 *          Maximum array size of [dest]. Note that the null-terminated char '\0' is also included.
 *      [num]
 *          Number of strings (in the va_list), that are going to be appended at the end of [dest] string.
 * 
 * @returns:
 *      The pointer to the first position of [dest] string after this function's finish.
 */
STRINGX_API char * Str_Concat (char * dest, size_t max_size, int num, ...) {

    /* (1) Declare a va_list object. The variable list (va_list) is represented by three dots (...). */
    va_list valist;

    /* (2) Set va_list's pointer to the LAST visible argument, i.e. [num]. The visible arguments are [dest],
     *     [max] and [num] in this case. The three dots (va_list) are placeholder for the UNvisible arguments.
     *     va_start(va_list, lastArg) initializes the va_list and set pointer to [num]. By calling va_arg the 
     *     first time, the pointer is increased and refers to the first element of val_list . By every next call
     *     of va_arg, the pointer is increased accordingly and refers to the next va_list element.
     */
    va_start(valist, num);

    // Set other pointer for the string [dest]
    char * pDest = dest;
    char * src = NULL;

    // Set this pointer to the last position (where the null-terminated char '\0' is).
    pDest += strlen(dest); // If strlen(dest) == 0, pointer pDest refers to first the position in string.

    /* (3) Use va_arg(valist, [type]) to iterate/access to the elements of va_list */
    while (num > 0) {
        src = va_arg(valist, char *); // Source string. Every va_arg is of type (char *)
        int free_space = (int)max_size - (1 + (int)strlen(dest)); // strlen() provides the length of string (number of chars)

        if (strlen(src) > free_space) {
            fprintf(stderr, "Error (Str_Concat): Overflow => The free space left in destination string is less than length of source string.\n");
            exit(EXIT_FAILURE);
        }
        else {
            //strncat(dest, src, strlen(src)); // Append string [src] to string [dest]
            while (*src) { // if every single char *src is not null ('\0'), copy characater from [src] to [dest]
                if (isprint(*src)) *pDest = *src;
                pDest++;
                src++;
            }
            *pDest = '\0';
        }
        num--;
    }

    //dest[strlen(dest)] = '\0'; // Null-terminated char: '\0' == 0 (End of string)
    pDest = '\0';

    /* (4) Clear all reserved memory for valist */
    va_end(valist);

    return &dest[0];
}

/*
 * Inserts a string [src] to the beginning of string [dest]. Argument [dest_max_size] indicates the maximum number
 * of (available) bytes of [dest] string.
 */
STRINGX_API char * Str_InsertFirst (char * src, char * dest, int dest_max_size) {
    int free_space = dest_max_size - (1 + (int)strlen(dest)); // strlen provides the number of char, 1 indicates the null-terminated byte '\0'
    int src_len = strlen(src);

    if (src_len > free_space) {
        fprintf(stderr, "Error (strcatx): Overflow => The free space left in destionary string is not enough for source string.\n");
        exit(EXIT_FAILURE);
    }

    for (int i = (strlen(dest)-1); i >= 0; i--) { dest[i+src_len] = dest[i]; }
    for (int i = 0; i < src_len; i++) { dest[i] = src[i]; }

    return dest;
}

/*
 * Allocate memory to create a copy of given [string].
 * Return pointer to the copy.
 * Note: Do not forget to free the allocated memory after using Duplicate_String function.
 */
STRINGX_API char * Duplicate_String (const char * __restrict__ string) {
    if (string == NULL) return NULL;
    size_t len = strlen(string) + 1; // number of chars in [string] plus null-terminated character '\0'
    char * strcopy = (char *) malloc(len); // allocates memory for the string duplicate
    if (strcopy == NULL) return NULL;
    strncpy(strcopy, string, len); // null-terminated char is included
    return strcopy;
}

/*
 * @params:
 *      [N] : Number of bytes (char) to be copied/duplicated (not including '\0')
 * @return:
 *      Pointer to the copy of [string]. * 
 * @Note: 
 *      Do not forget to free the allocated memory after using Duplicate_String function.
 */
STRINGX_API char * Duplicate_String_N (const char * __restrict__ string, size_t N) {
    if (string == NULL) return NULL;
    size_t len = strlen(string); // number of chars in [string]
    if (N >= len) { N = len; }
    char * strcopy = (char *) malloc(N+1); // allocates memory for the string duplicate, plus null-terminating character '\0'
    if (strcopy == NULL) return NULL;
    strncpy(strcopy, string, N);
    strcopy[N] = '\0';
    return strcopy;
}

#endif /* STRINGX_H */
