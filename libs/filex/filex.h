/*
 * filex.h
 */

#ifndef FILEX_H
#define FILEX_H

#define FILEX_API extern
#include "../stringx/stringx.h"

FILEX_API int Get_FileName (const char * __restrict__ filepath, char * __restrict__ output, size_t len);
FILEX_API int Get_FileRootName (const char * __restrict__ filepath, char * __restrict__ output, size_t len);
FILEX_API int Get_FileLocation (const char * __restrict__ filepath, char * __restrict__ output, size_t len);
FILEX_API int Read_File (const char * __restrict__ filepath, char * __restrict__ buffer);

//=============================================================================================================================
/*
 * Assume, a file path looks like "./path/to/filename.extension" or "C:\Path\to\filename.extension". 
 * This function will extract the string "filename.extension" (including extension after the dot (.)) 
 * and stores it into [output].
  * @params
 *      [output] : This string represents the place where the extracted filename.extension is stored in.
 *      [len] : Size of the string [output] including the null-terminated character (\0).
 */
FILEX_API int Get_FileName (const char * __restrict__ filepath, char * __restrict__ output, size_t len) {
    char * fpcopy = Duplicate_String(filepath); // Make a copy of string [filepath]    
    char delimiters[] = " /\\";
    char * current, * last; // Pointer to tokens
    current = strtok(fpcopy, delimiters); // Split and get first token
    if (current == NULL) {
        free(fpcopy);
        fprintf(stderr, "Warning (Get_Filename): The given string [filepath] is empty.\n");
        output = NULL;
        return 0; // FAILURE
    }
    // strtok splits the string [fpcopy] in tokens. The delimiters /, \\ and " " are replaced by space (or NULL). The return value refers to the first found token.
    while ( current ) { // while ( current != NULL ) 
        last = current;
        current = strtok(NULL, delimiters); // Next token
    }
    if ( last ==  NULL ) { last = current; }
    size_t n = strlen(last);
    if (len <= n) {
        free(fpcopy);
        fprintf(stderr, "Error (Get_Filename): The string length of [output] is not long enough for storing filename.\n");
        output = NULL;
        return 0; // FAILURE
    }
    strncpy(output, last, n);
    output[n] = '\0';
    free(fpcopy);
    return 1; // SUCCESS
}

/*
 * Assume, a file path looks like "./path/to/rootname.extension" or "C:\Path\to\rootname.extension". 
 * This function will extract the string "rootname" (NOT including file extension after the dot (.)) 
 * and stores it into [output].
  * @params
 *      [output] : This string represents the place where the extracted rootname (without extension) is stored in.
 *      [len] : Size of the string [output] including the null-terminated character (\0).
 */
FILEX_API int Get_FileRootName (const char * __restrict__ filepath, char * __restrict__ output, size_t len) {
    char * rootname = (char *) malloc(len);
    if ( !Get_FileName(filepath, rootname, len) ) {
        free(rootname);
        return 0; // FAILURE
    }
    char delimiter[] = ". "; // Dot in front of extension
    char * tok = strtok(rootname, delimiter); // Split string [rootname] in tokens and get first token    
    if (tok == NULL) {
        free(rootname);
        fprintf(stderr, "Warning (Get_Filename): The given string [filepath] is an empty string.\n");
        output = NULL;        
        return 0; // FAILURE
    }
    size_t n = strlen(tok);
    if (len <= n) { 
        free(rootname);
        fprintf(stderr, "Error (Get_Filename): The string length of [output] is not long enough for storing file rootname.\n");
        output = NULL;
        return 0; // FAILURE
    }
    strncpy(output, tok, n);
    output[n] = '\0';
    free(rootname);
    rootname = NULL;
    return 1;
}

/*
 * Provide only the location (path) to the file without the file name itself.
 * Assume, a file path looks like "./path/to/filename.extension" or "C:\Path\to\filename.extension". 
 * This function will extract the string "./path/to/" or "C:\Path\to\" (without the file name and its 
 * extension), and finally store this string in to [output].
 * 
 * @params:
 *      [output] : String object representing the file location, without including the file name.
 *      [len]    : Size of the destination string [output] where file location will be stored in.
 *                 The null-terminated character (\0) is already included in [len].
 */
FILEX_API int Get_FileLocation (const char * __restrict__ filepath, char * __restrict__ output, size_t len) {
    char * filename = (char *) malloc(len);
    int lo = 0; // strlen of [output]
    int cnt = 0; // counter for directory level
    if (!Get_FileName(filepath, filename, len)) {
        fprintf(stderr, "Error (Get_FileLocation): Cannot determine file location.\n");
        free(filename);
        return 0; // FAILURE
    }
    lo = strlen(filepath) - strlen(filename); // strlen of file location (WITHOUT file name)
    if (lo >= len) {
        fprintf(stderr, "Error (Get_FileLocation): Cannot determine file location. The reserved size for [output] is not long enough.\n");
        free(filename);
        return 0; // FAILURE
    }
    if (lo == 0 && len >= 3) {
        output[0] = '.';
        output[1] = '/';
        output[2] = '\0';
        cnt = 1;
    }
    else {
        for (int i = 0; i < lo; i++) {
            output[i] = filepath[i];
            if (output[i] == '/' || output[i] == '\\') { cnt += 1; }
        }
        output[lo] = '\0';
    }
    free(filename);
    return cnt; // SUCCESS
}

/*
 * Read content of file [filepath] and copy its content to [buffer].
 * Returns EXIT_SUCCESS (0) if SUCCESS. Otherwise, return EXIT_FAILURE (1) if FAILURE.
 */
FILEX_API int Read_File (const char * __restrict__ filepath, char * __restrict__ buffer) {
    if (!filepath) { fprintf(stderr, "Error (Read_JsonFile): No file name is given\nUsage: ./<Prog> <Filename>\n"); return 0; }

    FILE * fp = NULL;
    char * local_buffer = NULL;
    int ret = 0;
    int len;
    
    /* Open file stream/pointer */
    fp = fopen(filepath, "r");
    if (fp == NULL) { fprintf(stderr, "Error (Read_JsonFile): File %s cannot be opened.\n", filepath); return EXIT_FAILURE; }

    /* Read whole file content into [local_buffer] */
    ret = fseek(fp, 0, SEEK_END); // moves file pointer to EOF
    if (ret) { fprintf(stderr, "Error (Read_JsonFile): fseek::Cannot set file pointer to EOF.\n"); return EXIT_FAILURE; }

    len = ftell(fp); // returns current position (= number of bytes from 0 to EOF) of file pointer fp
    if (len == -1) { fprintf(stderr, "Error (Read_JsonFile): ftell::Cannot determine current position of file pointer.\n"); return EXIT_FAILURE; }

    ret = fseek(fp, 0, SEEK_SET); // moves file pointer to beginning of file
    if (ret) { fprintf(stderr, "Error (Read_JsonFile): fseek::Cannot set file pointer to beginning of file.\n"); return EXIT_FAILURE; }

    local_buffer = (char *) malloc(len);
    if (local_buffer) { // local_buffer != NULL
        fread(local_buffer, 1, len, fp); // reads all [len] bytes from stream (fp) into [local_buffer]. Every read byte has size of exactly 1 byte.
    }

    /* Close file stream */
    if (fclose(fp) != 0) { fprintf(stderr, "Error (Read_JsonFile): fclose::File %s cannot be closed correctly.\n", filepath); return EXIT_FAILURE; }

    /* Copy from [local_buffer] to [buffer] adn fill the rest of [buffer] with '\0' */
    strncpy(buffer, local_buffer, len);

    free(local_buffer);

    return EXIT_SUCCESS;
}
#endif /* FILEX_H */
