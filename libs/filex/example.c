/*
 * example.c
 */
#include <stdio.h>
#include "filex.h"

int main(int argc, char * argv[]) {
    if (argc < 2) {
        printf("\nWarning: No filepath is given. Please give a file path.\n");
        printf("USAGE: ./example  <filepath>\nEXIT\n\n");
        return 0;
    }
    
    int len = 20;
    char * filepath = argv[1];
    char * output = (char *) malloc(len);

    Get_FileLocation(filepath, output, len);

    printf("Output: %s\n", output);

    free(output);

    return 0;
}
 