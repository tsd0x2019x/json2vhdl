/*
 * rex.c
 * 
 *      $ gcc -Wall -Werror -std=gnu99 -pedantic <file.c> -o <output>
 * 
 * POSIX RegEx in C:
 * http://web.archive.org/web/20160308115653/http://peope.net/old/regex.html
 * https://stackoverflow.com/questions/1085083/regular-expressions-in-c-examples
 * https://linux.die.net/man/3/regcomp
 */

#include <stdio.h>
#include <stdlib.h>
#include <regex.h>

int main (int argc, char * argv[]) {

    regex_t regex;
    int reti;
    char errbuff[100];
    char * pattern = "a[0-9]+b"; // a123b
    char * string = argv[1];

    if (string == NULL) {
        fprintf(stderr, "Error: Argument is empty.\nUsage: ./rex <pattern>\n");
        exit(EXIT_FAILURE);
    }

    /* Prepare and compile regular expression */
    reti = regcomp(&regex, pattern, REG_EXTENDED);
    if ( reti ) {
        fprintf(stderr, "Could not compile regex.\n");
        exit(EXIT_FAILURE);
    }

    /* Execute regular expression */
    reti = regexec(&regex, string, 0, NULL, 0);
    if ( !reti ) {
        printf("Match.\n");
    }
    else if ( reti == REG_NOMATCH ) {
        printf("No match.\n");
    }
    else {
        /* Report error if regular expression matching failed */
        regerror(reti, &regex, errbuff, sizeof(errbuff));
        fprintf(stderr, "Error: Regex match failed => %s\n", errbuff);
    }

    /* Free compiled regular expression to use regex_t again */
    regfree(&regex);

    return EXIT_SUCCESS;
}