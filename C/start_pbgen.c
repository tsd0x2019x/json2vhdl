/*
 * start_pbgen.c
 * 
 * Compile in Linux using GCC:
 *      /usr/bin/gcc
 * 
 * Cross-compile for Windows using Mingw-w64:
 *      32-Bit: /usr/bin/i686-w64-mingw32-gcc
 *      64-Bit: /usr/bin/x86_64-w64-mingw32-gcc
 * 
 * Install Mingw-w64:
 *      sudo apt-get install mingw-w64
 */

#include <stdio.h>
#include "JsonVhdl.h"

/*
 * Show help menue and usage with options.
 */
void Display_Help (const char * program_name) {
    const char * pName = program_name;
    const char json_filename[] = "AXIL_HWIO.json";
    if (*pName == '.' && pName[1] == '/') {
        pName += 2;
    }

    printf("USAGE:   %s [json_filename | option]\n", program_name);
    printf("DESCRIPTION: This program reads from a JSON file and generates many VHD files containing Vhdl code.\n");
    printf("OPTIONS:\n");
    printf("    -c, --clean   : Auto-remove all generated vhd/vhdl files that are in the current directory as the calling program.\n");
    printf("    -h, --help    : Show help.\n");
    printf("    -?            : Show help.\n");
    printf("EXAMPLE:\n");
    printf("  Assume that the calling program is \"%s\" and the json file is \"%s\". In the following are some use cases:\n", pName, json_filename);
    printf("    (1) Start generating vhdl files from json:\n");
    printf("            %s %s\n", program_name, json_filename);
    printf("    (2) Auto-remove all generated vhd/vhdl files:\n");
    printf("            %s --clean\n", program_name);
    printf("            %s -c\n", program_name);
    printf("    (3) Show help and usage menue:\n");
    printf("            %s --help\n", program_name);
    printf("            %s -h\n", program_name);
    printf("            %s -?\n\n", program_name);
    printf("Let the path to \"%s\" be either (Linux) \"/home/path/to/%s\" or (Windows) \"C:\\Path\\To\\%s\".\n", json_filename, json_filename, json_filename);
    printf("For the sake of faultness use, surround the json_filename with quote sign (\"\"). E.g.:\n");
    printf("            %s \"/home/path/to/%s\"\n", program_name, json_filename);
    printf("            %s \"C:\\Path\\To\\%s\"\n", program_name, json_filename);
    printf("Or replace all Backslashes (\\) in json_filename by Slashes (/). E.g.:\n");
    printf("            %s \"C:/Path/To/%s\"\n", program_name, json_filename);
    printf("If not use quote sign (\"\"), use the Double-Backslash (\\\\) to escape all Backslashes (\\) in json_filename. E.g.:\n");
    printf("            %s C:\\\\Path\\\\To\\\\%s\n", program_name, json_filename);
    printf("\n");
}

/*
 * Main program. Start from here.
 */
int main (int argc, char * argv[]) {
    
    if (argc < 2) {
        printf("\nWARNING: No json file name is given.\n\n");
        Display_Help(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (strncmp(argv[1], "-c", 2) == 0 || strncmp(argv[1], "--clean", 7) == 0) { // Clear all recently generated vhd files
        char command[] = "rm -f *.vhd"; // command string
        FILE * pipe; // pipe
        /* Create pipe and invoke remove-command */
        pipe = popen(command, "r");
        /* Command successfully invoked. Close pipe. Free command pointer */
        fclose(pipe);
        printf("All vhdl files are auto-removed.\n");
    }
    else if (strncmp(argv[1], "-h", 2) == 0 || strncmp(argv[1], "--help", 6) == 0 || strncmp(argv[1], "-?", 2) == 0) {
        Display_Help(argv[0]);
    }
    else {
        Replace_Char_In_String(argv[1], '\\', '/');
        Start_PbGen(argv[1]);
    }
   
    return EXIT_SUCCESS;
}