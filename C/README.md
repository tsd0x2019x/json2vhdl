================================================================================
# HOW TO RUN AND GENERATE VHDL CODE FROM JSON
================================================================================

1. Windows:
	1.1 Run "start_pbgen.exe" followed by a json file name:

			$ start_pbgen.exe AXIL_HWIO.json
			
2. Linux (Ubuntu):
	2.1 Run the binary "start_pbgen" followed by a json file name:

			$ ./start_pbgen AXIL_HWIO.json


================================================================================
# HOW TO CROSS-COMPILE (GENERATE .exe file) FOR WINDOWS
================================================================================

0. I do not compile source immediately in Windows, but in Linux (Ubuntu).

1. For that purpose, I installed Mingw-w64 for Linux:

		$ sudo apt-get install mingw-w64
	
2. Afterwards, one can just type "make" to compile:

		$ make
		
   In the Makefile, all options for cross-compiling are made. It creates two files:

		a) "start_pbgen" for Linux, and

		b) "start_pbgen.exe" for Windows.

================================================================================
# ADDITIONAL INFORMATION
================================================================================		
Compile in Linux using GCC:

	/usr/bin/gcc

Cross-compile for Windows using Mingw-w64:

	32-Bit: /usr/bin/i686-w64-mingw32-gcc

	64-Bit: /usr/bin/x86_64-w64-mingw32-gcc