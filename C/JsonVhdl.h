/*
 * JsonVhdl2.h
 */
#ifndef JSONVHDL_H
#define JSONVHDL_H

#include <math.h>
#include <time.h>
#include "jsmn.h"
#include "../libs/stringx/stringx.h"
#include "../libs/filex/filex.h"

#define JSONVHDL_API extern
#define MAX_STR 640
#define MAX_TOK 128
#define LEN_STR 120
#define LEN_FILENAME 80

typedef struct parbank_data_t parbank_data_t; // forward declaration
struct parbank_data_t {
    char name[LEN_STR];
    char path[LEN_STR];
    unsigned int offset;
    char file[LEN_STR];
    unsigned int size;
    parbank_data_t * next;
};

typedef struct {
    unsigned int size; // Number of elements (parbank_data_t) in the list
    parbank_data_t * begin;
    parbank_data_t * end;
} parbank_list_t;

static int iProcessCounter = 0;

JSONVHDL_API void Configure_FilePath (const char * __restrict__ AXILFileLocation, const char * __restrict__ TemplateFilePath, char * __restrict__ ResultPath, size_t iResultPathLength);
JSONVHDL_API parbank_list_t * Malloc_Parbank_List ();
JSONVHDL_API void Free_Parbank_List (parbank_list_t ** list);
JSONVHDL_API unsigned int AddDataBack_Parbank_List (parbank_list_t * src_list, char * name_str, char * path_str, unsigned int offset, char * file_str, unsigned int file_size);

JSONVHDL_API int JsonEquals(const char * json, jsmntok_t * tok, const char * string);
JSONVHDL_API int Get_ValueOfKey (const char * json, jsmntok_t * tokens, size_t num, const char * key, char * value);
JSONVHDL_API int Get_TokenString (const char * json, jsmntok_t * tokens, size_t num_tok, int index, char * buffer);
JSONVHDL_API int Get_TokenObjectToString (const char * json, jsmntok_t * tokens, size_t num_tok, int index, char * buffer);
JSONVHDL_API int Get_TokenPrimitiveNumber (const char * json, jsmntok_t * tokens, size_t num_tok, int index, int * number);
JSONVHDL_API int Compare_TokenStrings(const char * json, jsmntok_t * tokens, size_t num_tok, int index1, int index2);
JSONVHDL_API int New_JsonParse (const char * src_json, jsmntok_t * dst_tokens);
JSONVHDL_API int New_JsonParseFromFile (const char * filepath, char * dst_json, jsmntok_t * dst_tokens);

JSONVHDL_API void Generate_Vhdl_Header (FILE * fp, const char * title);
JSONVHDL_API void Generate_Parset (const char * json_filename, const char * module_name, const char * submodule_name);
JSONVHDL_API void Generate_Vhdl_Parset (const char * json, jsmntok_t * tokens, size_t num_tok, char * fileLocation);
JSMN_API void Generate_Axi_Parbank (parbank_list_t * pbList, char * module_name);
JSMN_API void Generate_Parbank (parbank_list_t * pbList, char * module_name);
void Calculate_Parbank_Length (const char * filepath, int * dst_length);
JSMN_API void Generate_Parbank_Records (FILE * fp, parbank_list_t * pbList);
JSONVHDL_API void Generate_Vhdl_Parbank (const char * json, jsmntok_t * tokens, size_t num_tok, char * fileLocation);
JSONVHDL_API void Generate_Parset_Records (FILE * fp, const char * json_filename, const char * submodule_name);
JSONVHDL_API void Generate_Vhdl_Records (const char * json, jsmntok_t * tokens, size_t num_tok, char * fileLocation);
JSONVHDL_API void Start_PbGen (const char * filepath);

//=============================================================================================================================
/*
 * @params:
 *      [AXILFileLocaton]
 *          Location of the "AXIL_HWIO.json" file. Not including the "AXIL_HWIO.json" file name. For instance, if the full path
 *          of "AXIL_HWIO.json" is "/path/to/location/of/AXIL_HWIO.json", then [AXILFileLocation] only contains 
 *          "/path/to/location/of/" and does not include the name "AXIL_HWIO.json".
 *      [TemplateFilePath]
 *          Contains the path to a JSON Template file. This path can be absoulte (full), e.g. "/path/to/template/file.json", or 
 *          relative, e.g. "./location/template/file.json".
 *      [ResultPath]
 *      [iResultPathLength]
 *          Includes null-terminated character '\0'
 */
JSONVHDL_API void Configure_FilePath (const char * __restrict__ AXILFileLocation, const char * __restrict__ TemplateFilePath, char * __restrict__ ResultPath, size_t iResultPathLength) {
    /* Make a copy of AXILFileLocation */
    size_t iAXILFileLen = strlen(AXILFileLocation);
    char * CopyAXILFileLocation = (char *) malloc(iAXILFileLen + 1); // strlen + '\0'
    strncpy(CopyAXILFileLocation, AXILFileLocation, iAXILFileLen);
    CopyAXILFileLocation[iAXILFileLen] = '\0';
    /* Some pointers to (copy of) AXILFileLocation and TemplateFilePath */
    const char * pTemp = TemplateFilePath;
    int iNumberOfParents = 0;
    /* Distinction of cases */
    switch ( *TemplateFilePath ) {
        case '.':
            ++pTemp;
            if (*pTemp != '.') { // ./path/to/template/file.json
                pTemp++;
            }
            else { // ../path/to/template/file.json, or ../../path/to/template/file.json, or ../../p/t/template/file.json
                iNumberOfParents = 1;
                while (pTemp[1] == '/' && pTemp[2] == '.' && pTemp[3] == '.') { // ../../path/to/template/file.json
                    iNumberOfParents += 1;
                    pTemp += 3;
                }
                if (*pTemp == '.') {
                    pTemp += 2;
                }
                else {
                    do {
                        --pTemp;
                    } while ( *pTemp != '/' );
                    pTemp++;
                }
            }
            // Extract path (i.e. without file name) from full path
            char * pc = CopyAXILFileLocation;
            pc += iAXILFileLen; // points to last character ('\0') of string
            while (iNumberOfParents > 0) { // Navigate to parent-dictories
                do {
                    --pc;
                } while ( *pc != '/' );
                iNumberOfParents -= 1;
            }
            pc[1] = '\0';
            // Build and concatenate new path
            Str_Concat(ResultPath, iResultPathLength, 2, CopyAXILFileLocation, pTemp);
            break;
        case '/':
            Str_Concat(ResultPath, iResultPathLength, 1, pTemp);
            break;
        default:
            if ( (*pTemp >= 48 && *pTemp <= 57) || (*pTemp >= 65 && *pTemp <= 90) || (*pTemp >= 97 && *pTemp <= 122) ) {
                if ( pTemp[1] != ':' ) {
                    Str_Concat(ResultPath, iResultPathLength, 2, CopyAXILFileLocation, pTemp);
                }
                else { // C:/Path/To/File.extension
                    Str_Concat(ResultPath, iResultPathLength, 1, pTemp);
                }
            }
            break;
    }
    free(CopyAXILFileLocation);
}

//=============================================================================================================================
/*
 *
 */
JSONVHDL_API parbank_list_t * Malloc_Parbank_List () {
    parbank_list_t * list = (parbank_list_t *) malloc(sizeof(parbank_list_t));
    list->size = 0;
    list->begin = NULL;
    list->end = list->begin;
    return list;
}

/*
 *
 */
JSONVHDL_API void Free_Parbank_List (parbank_list_t ** list) {
    if (*list != NULL) {
        parbank_data_t * current;
        while ((current = (*list)->begin) != NULL) {
            (*list)->begin = ((*list)->begin)->next;
            current->next = NULL;
            free(current);
        }
        (*list)->end = (*list)->begin; // NULL
        free(*list);
        *list = NULL;
    }
}

/*
 *
 */
JSONVHDL_API unsigned int AddDataBack_Parbank_List (parbank_list_t * src_list, char * name_str, char * path_str, unsigned int offset, char * file_str, unsigned int file_size) {
    parbank_data_t * pData = NULL;
    if (src_list->size == 0) {
        src_list->begin = (parbank_data_t *) malloc(sizeof(parbank_data_t));
        src_list->end = src_list->begin;
        pData = src_list->begin;
    }
    else {
        (src_list->end)->next = (parbank_data_t *) malloc(sizeof(parbank_data_t));
        src_list->end = (src_list->end)->next;
        (src_list->end)->next = NULL;
        pData = src_list->end;
    }
    (src_list->size)++;
    strncpy(pData->name, name_str, strlen(name_str)); pData->name[strlen(name_str)] = '\0';
    strncpy(pData->path, path_str, strlen(path_str)); pData->path[strlen(path_str)] = '\0';
    pData->offset = offset;
    strncpy(pData->file, file_str, strlen(file_str)); pData->file[strlen(file_str)] = '\0';
    pData->size = file_size;
    return src_list->size;
}

//=============================================================================================================================
/*
 * Compare the strings in [tok] and [string].
 * Returns non-zero positive number if SUCCESS. Otherwise, zero if FAILURE.
 */
JSONVHDL_API int JsonEquals(const char * json, jsmntok_t * tok, const char * string) {
    if (tok->type == JSMN_STRING && (int)strlen(string) == tok->end - tok->start && 
        strncmp(json + tok->start, string, tok->end - tok->start) == 0) {
        return 1;
    }
    return 0;
}

/*
 * Provide the value (string) by a given  [key] from key/value pair. The found value string is 
 * the stored into [value].
 * Returns non-zero positive number if SUCCESS. Otherwise, zero if FAILURE.
 */
JSONVHDL_API int Get_ValueOfKey (const char * json, jsmntok_t * tokens, size_t num, const char * key, char * value) {
    for (int i = 1; i < num; i++) { // begins at i = 1 (not 0), since i = 0 is index of object that contains the whole json file
        if (JsonEquals(json, &tokens[i], key)) {
            strncpy(value, json + tokens[i+1].start, tokens[i+1].end - tokens[i+1].start);
            return 1;
        }
    }
    return 0;
}

/*
 * Get the string value of the token with index [index]. The obtained string value is stored in [buffer].
 * Returns non-zero positive number for SUCCESS (i.e. if the current token at index [index] is JSMN_STRING). 
 * Otherwise, return zero (0) for FAILURE (i.e. if the current token at index [index] is neither JSMN_STRING).
 */
JSONVHDL_API int Get_TokenString (const char * json, jsmntok_t * tokens, size_t num_tok, int index /* token index */, char * buffer) {
    if (index < 0 || index >= num_tok) { 
        fprintf(stderr, "Error (Get_TokenString): Index (%d) out of bound.\n", index);
        return 0;
    }
    if (tokens[index].type == JSMN_STRING) {
        strncpy(buffer, (json + tokens[index].start), (tokens[index].end - tokens[index].start));
        buffer[tokens[index].end - tokens[index].start] = 0; // '\0';
        return 1;
    }
    // No token found.
    return 0;
}

/*
 * Converts the content within a token[index] into string object. The string result is stored in [buffer].
 * Returns non-zero positive number for SUCCESS (i.e. if the current token at index [index] is JSMN_OBJECT or JSMN_STRING). 
 * Otherwise, return zero (0) for FAILURE (i.e. if the current token at index [index] is neither JSMN_OBJECT nor JSMN_STRING).
 */
JSONVHDL_API int Get_TokenObjectToString (const char * json, jsmntok_t * tokens, size_t num_tok, int index /* token index */, char * buffer) {
    if (index < 0 || index >= num_tok) { 
        fprintf(stderr, "Error (Get_TokenObjectToString): Index (%d) out of bound.\n", index);
        return 0;
    }
    if (tokens[index].type == JSMN_STRING || tokens[index].type == JSMN_OBJECT) {
        strncpy(buffer, (json + tokens[index].start), (tokens[index].end - tokens[index].start));
        buffer[tokens[index].end - tokens[index].start] = '\0';
        return 1;
    }
    // No token found.
    return 0;
}

/*
 *
 */
JSONVHDL_API int Get_TokenPrimitiveNumber (const char * json, jsmntok_t * tokens, size_t num_tok, int index /* token index */, int * number) {
    if (index < 0 || index >= num_tok) { 
        fprintf(stderr, "Error (Get_TokenString): Index (%d) out of bound.\n", index);
        return 0;
    }
    if (tokens[index].type == JSMN_PRIMITIVE) {
        char * copy = Duplicate_String_N(json + tokens[index].start, tokens[index].end - tokens[index].start);
        *number = atoi(copy);
        free(copy);
        return 1;
    }
    // No token found
    return 0;
}

/*
 * Compare if strings of tokens[index1] and tokens[index2] are equal.
 * Return non-zero (1) value if EQUAL. Otherwise, return zero (0).
 */
JSONVHDL_API int Compare_TokenStrings(const char * json, jsmntok_t * tokens, size_t num_tok, int index1, int index2) {
    if (tokens[index1].type != JSMN_STRING || tokens[index2].type != JSMN_STRING) { return 0; }
    
    char tok_str1[LEN_STR] = "";
    char tok_str2[LEN_STR] = "";
    Get_TokenString(json, tokens, num_tok, index1, tok_str1);
    Get_TokenString(json, tokens, num_tok, index2, tok_str2);

    return (strcmp(tok_str1, tok_str2)) ? 0 : 1;
}

/*
 *
 */
JSONVHDL_API int New_JsonParse (const char * src_json, jsmntok_t * dst_tokens) {
    //char json[MAX_STR] = ""; // stores the content of json file [file_json]
    jsmn_parser parser; // Json parser
    //jsmntok_t tokens[MAX_TOK];
    int num_tok = 0;

    /* Initialize jsmn/json parser */
    jsmn_init(&parser);
    // Number of calculated jsmn tokens from read json file
    num_tok = jsmn_parse(&parser, src_json, strlen(src_json), dst_tokens, MAX_TOK);

    if (num_tok <= 1) {
        if (num_tok == JSMN_ERROR_PART) fprintf(stderr, "Error (New_JsonParse): -3 => The string is not a full JSON packet, more bytes expected.\n");
        else if (num_tok == JSMN_ERROR_INVAL) fprintf(stderr, "Error (New_JsonParse): -2 => Invalid character inside JSON string.\n");
        else fprintf(stderr, "Error (New_JsonParse): There are not enough tokens provided.\n");
        exit(EXIT_FAILURE);
    }

    return num_tok;
}


/*
 *
 */
JSONVHDL_API int New_JsonParseFromFile (const char * filepath, char * dst_json, jsmntok_t * dst_tokens) {
    /* Read file [file_json] and store its content to [dst_json] */
    if (Read_File(filepath, dst_json) != EXIT_SUCCESS) { exit(EXIT_FAILURE); }

    /* Read from [dst_json] to parse json. Then store generated tokens to [dst_tokens] */
    return New_JsonParse(dst_json, dst_tokens);
}
//=============================================================================================================================
/*
 * Generate header for vhdl file
 */
JSONVHDL_API void Generate_Vhdl_Header (FILE * fp, const char * title) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    fputs(      "-------------------------------------------------------------------------------\n", fp);
    fprintf(fp, "-- Title      : %s                                                   \n", title);
    fputs(      "-- Project    : NG                                                             \n", fp);
    fputs(      "-------------------------------------------------------------------------------\n", fp);
    fprintf(fp, "-- File       : %s.vhd                                       \n", title);
    fputs(      "-- Company    : Micronova Ag / Ks / Cz                                         \n", fp);
    fprintf(fp, "-- Created    : %d-%02d-%02d %02d:%02d:%02d      \n", (tm.tm_year + 1900), (tm.tm_mon + 1), tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    fputs(      "-------------------------------------------------------------------------------\n", fp);
    fputs(      "-- Description: autogenerated file by pbgen tool                               \n", fp);
    fputs(      "                                                                               \n", fp);
    fputs(      "--              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                 \n", fp);
    fputs(      "--              !!!!!    DO NOT EDIT      !!!!                                 \n", fp);
    fputs(      "--              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                 \n", fp);
    fputs(      "-------------------------------------------------------------------------------\n", fp);
    fputs(      "-- Copyright (c) 2017                                                          \n", fp);
    fputs(      "-------------------------------------------------------------------------------\n", fp);
    fputs(      "                                                                               \n", fp);
    fputs(      "library ieee;                                                                  \n", fp);
    fputs(      "use ieee.std_logic_1164.all;                                                   \n", fp);
    fputs(      "use ieee.numeric_std.all;                                                      \n", fp);
    fputs(      "library work;                                                                  \n", fp);
    fputs(      "                                                                               \n", fp);
}

/*
 *
 */
JSONVHDL_API void Generate_Parset (const char * json_filename, const char * module_name, const char * submodule_name) {
    /* Declare file pointer */
    FILE * fp = NULL;
    char header_title[LEN_FILENAME] = "parbank_";
    char entity_name[LEN_FILENAME] = "";
    char vhd_filename[LEN_FILENAME] = "";
    
    /* Create name for *.vhd file and title for header */
    strncpy(entity_name, "parset_", LEN_FILENAME);
    Str_Concat(entity_name, LEN_FILENAME, 1, submodule_name);
    strncpy(vhd_filename, entity_name, strlen(entity_name));
    //Str_Concat(header_title, LEN_FILENAME, 1, entity_name);
    //Str_Concat(header_title, LEN_FILENAME, 1, "_pkg");
    //Str_Concat(vhd_filename, LEN_FILENAME, 1, ".vhd");
    Str_Concat(header_title, strlen(header_title) + strlen(entity_name) + 1, 1, entity_name);    
    Str_Concat(header_title, strlen(header_title) + 5, 1, "_pkg"); // strlen of "_pkg" is 4, plus 1 for '\0' = 5
    Str_Concat(vhd_filename, strlen(vhd_filename) + 5, 1, ".vhd"); // strlen of ".vhd" is 4, plus 1 for '\0' = 5

    printf("Generate_Vhdl_Parset...\nGenerate \"%s.vhd\" =>", entity_name);

    fp = fopen(vhd_filename, "w");

    /* Generate the header */
    Generate_Vhdl_Header(fp, header_title);

    fputs("use work.pbgen_pkg.all;\n", fp);
    fprintf(fp, "use work.parbank_%s_pkg.all;\n\n", module_name);

    /* Generate the entity */
    fprintf(fp, "entity %s is\n", entity_name);
    fputs("    port (\n", fp);

    fputs("        mm_pb_i      : in  REC_MM_PB_MOSI; -- memory mapped record for master out, slave in\n", fp);
    fputs("        mm_pb_o      : out REC_MM_PB_MISO; -- memory mapped record for master in, slave out\n\n", fp);

    fprintf(fp, "        PAR_i        : in  REC_PARSET_%s_IN;      -- record for input parameters to parameterbank\n", submodule_name);
    fprintf(fp, "        PAR_o        : out REC_PARSET_%s_OUT      -- record for output parameters from parameterbank\n", submodule_name);
  
    fputs("  );\n", fp);
    fprintf(fp, "end entity %s;\n\n", entity_name);

    /* Generate the architecture */
    fprintf(fp, "architecture rtl of %s is\n\n", entity_name);
    fputs("signal reset_n_s  : std_logic;\n\n", fp);

    /*
     * Define the signals
     */
    char json[MAX_STR] = ""; // stores the content of json file [file_json]
    jsmntok_t tokens[MAX_TOK];    
    int num_tok = New_JsonParseFromFile(json_filename, json, tokens);
    int i = 1, paramIndex = 0;
    int foundParameters = 0;
    // Retrieves for token[i] (value) of the key "parameters"
    for (; i < num_tok; i++) { 
        char tok_str[LEN_STR] = "";
        //memcpy(tok_str, "", LEN_STR);
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue;
        if (strcmp(tok_str, "parameters") != 0) continue;
        foundParameters = 1;
        break; // if index of token[i] (value) for the key "parameters" is found, break loop. Remember index i
    }
    if (!foundParameters) {
        fprintf(stderr, "Error (Generate_Vhdl_Parset): There is no key \"parameters\" found in TemplateNG_ json file.\n");
        return;
    }
    paramIndex = i; // Remember index i of found key (token) "parameters"
    i = paramIndex + 2; // refers to the first token (key) within the value bundle of found key "parameters"
    int j = 0;
    for (; i < (num_tok-1); ) { // Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        int width = 0;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw", "B_type_ro", ...
            j = i + 2;
            for (;;) {
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0 },
                if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get value of key "mode"
                }
                j += 2;
            }
            i = j;
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (strcmp(mode_str, "wo") == 0 || strcmp(mode_str, "rw") == 0 || strcmp(mode_str, "wog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "signal %s_we_s    : std_logic;\n", reg_name);
                fprintf(fp, "signal %s_data_s  : std_logic_vector(%d downto 0);\n", reg_name, width-1);
            }
            if (strcmp(mode_str, "ro") == 0 || strcmp(mode_str, "rw") == 0 || strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "signal %s_re_s : std_logic;\n", reg_name);
            }
            if (strcmp(mode_str, "mm") == 0) {
                fprintf(fp, "signal %s_we_s : std_logic;\n", reg_name);
            }
        }
    }
    fputs("\nbegin\n\n", fp);
    fputs("reset_n_s <= mm_pb_i.resn and not(PAR_i.parameter_reset);\n", fp);
    
    /*
     * Instantiate register slices for write
     */
    int address = 0;
    i = paramIndex + 2; // refers to the first token (key) within the value bundle of found key "parameters"
    j = 0;
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        int width = 0;
        int size = 1;
        int reset = 0;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            j = i + 2;
            for (;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get value of key "mode"
                }
                else if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &size); // Get value of key "size"
                } 
                else if (strcmp(tok_str, "reset") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &reset); // Get value of key "reset"
                } 
                j += 2;
            } // end-for
            i = j;
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (strcmp(mode_str, "wo") == 0 || strcmp(mode_str, "rw") == 0) {
                fprintf(fp, "\n%s_we_s <= '1' when mm_pb_i.addr=x\"%08x\" and mm_pb_i.wren='1' and mm_pb_i.csel='1' else '0'; \n", reg_name, address);
                fprintf(fp, "reg_%s : entity work.pbgen_regslice\n", reg_name);
                fputs("generic map (\n", fp);
                fprintf(fp, "    width  => %d,\n", width);
                fprintf(fp, "    resval => x\"%08x\"\n", reset);
                fputs(")\n", fp);
                fputs("port map (\n", fp);
                fputs("  clk_i    => mm_pb_i.clk ,\n", fp);
                fputs("  resetn_i => reset_n_s,\n", fp);
                fprintf(fp, "  we_i     => %s_we_s,\n", reg_name);
                fprintf(fp, "  data_i   => mm_pb_i.data(%d-1 downto 0),\n", width);
                fprintf(fp, "  data_o   => %s_data_s\n", reg_name);
                fputs(");\n", fp);

                if (width == 1) {
                    fprintf(fp, "\nPAR_o.%s <= %s_data_s(0);\n", reg_name, reg_name);
                } else {
                    fprintf(fp, "\nPAR_o.%s <= %s_data_s(%d-1 downto 0);\n", reg_name, reg_name, width);
                } 
            }
            if (strcmp(mode_str, "wog") == 0 || strcmp(mode_str, "rwg") == 0 || strcmp(mode_str, "mm") == 0) {
                if (width == 1) {
                    fprintf(fp, "\nPAR_o.%s     <= mm_pb_i.data(0);\n", reg_name);
                } else {
                    fprintf(fp, "PAR_o.%s     <= mm_pb_i.data(%d-1 downto 0);\n", reg_name, width);
                }
            }
            if (strcmp(mode_str, "wog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "PAR_o.%s_wr  <= '1' when mm_pb_i.addr=x\"%08x\" and mm_pb_i.wren='1' and mm_pb_i.csel='1' else '0';\n", reg_name, address);
            }
            if (strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "PAR_o.%s_rd  <= '1' when mm_pb_i.addr=x\"%08x\" and mm_pb_i.rden='1' and mm_pb_i.csel='1' else '0';\n", reg_name, address);
            }
            if (strcmp(mode_str, "mm") == 0 ) {
                fprintf(fp, "\nPAR_o.%s_wr <= '1' when mm_pb_i.addr>=x\"%08x\" and\n", reg_name, address);
                fprintf(fp, "                                     mm_pb_i.addr<x\"%08x\"\n", address + size);
                fputs("                                     and mm_pb_i.wren='1' and mm_pb_i.csel='1' else '0'; \n", fp);
                fprintf(fp, "\nPAR_o.%s_rd <= '1' when mm_pb_i.addr>=x\"%08x\" and\n", reg_name, address);
                fprintf(fp, "                                     mm_pb_i.addr<x\"%08x\"\n", address + size);
                fputs("                                     and mm_pb_i.rden='1' and mm_pb_i.csel='1' else '0'; \n", fp);
      
                if (size > 1) {
                    //append result "\nPAR_o.[subst $name]_addr <= mm_pb_i.addr([expr int([ld $size])-1] downto 0);\n"
		            fprintf(fp, "\nPAR_o.%s_addr <= std_logic_vector(resize(shift_right(unsigned(mm_pb_i.addr) - x\"%08x\" ,2), PAR_o.%s_addr'length));\n", reg_name, address, reg_name);
                }
            }
            address += size * 4; // Tcl: incr address [expr $size*4]
        }
    }

    /*
     * Mux the read data
     */
    address = 0;
    fputs("\n  mm_pb_o.data <=\n", fp);
    i = paramIndex + 2; // refers to the first token (key) within the value bundle of found key "parameters"
    j = 0;
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        char zeros_str[LEN_STR] = "";
        int width = 0;
        int size = 1;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            j = i + 2;
            for (;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get value of key "mode"
                }
                else if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &size); // Get value of key "size"
                } 
                j += 2;
            } // end-for
            i = j;
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (width < 32) {
                // set zeros "[format \"%0[subst [expr 32-$width]]d\" 0] &"
                sprintf(zeros_str, "\"%0*d\" &", 32 - width, 0);
            } 
            //else { set zeros "" }
            if (strcmp(mode_str, "rw") == 0) {
                fprintf(fp, "    %s %s_data_s when mm_pb_i.addr=x\"%08x\" else\n", zeros_str, reg_name, address);
            }
            if (strcmp(mode_str, "ro") == 0) {
                fprintf(fp, "    %s PAR_i.%s when mm_pb_i.addr=x\"%08x\" else\n", zeros_str, reg_name, address);
            }
            if (strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "    %s PAR_i.%s when mm_pb_i.addr=x\"%08x\" else\n", zeros_str, reg_name, address);
            }
            if (strcmp(mode_str, "mm") == 0) {
                //set size [dict get $value size]
                fprintf(fp, "    %s PAR_i.%s when mm_pb_i.addr>=x\"%08x\"\n", zeros_str, reg_name, address);
                fprintf(fp, "                             and mm_pb_i.addr<x\"%08x\" else\n", address + size);
            }
            address += size * 4; //incr address [expr $size*4]
        }
    }
    fputs("    x\"00000000\";\n\n", fp);

    /*
     * Generate the ready status
     */
    address = 0;
    fputs("\n  mm_pb_o.rdy  <=\n", fp);
    i = paramIndex + 2; // refers to the first token (key) within the value bundle of found key "parameters"
    j = 0;
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        int width = 0;
        int size = 1;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            j = i + 2;
            for (;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get value of key "mode"
                }
                else if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &size); // Get value of key "size"
                } 
                j += 2;
            } // end-for
            i = j;
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "    not PAR_i.%s_busy when mm_pb_i.addr=x\"%08x\" else  -- %s\n", reg_name, address, reg_name);
            }
            if (strcmp(mode_str, "mm") == 0) {
                //set size [dict get $value size]
                fprintf(fp, "    not PAR_i.%s_busy when (mm_pb_i.addr>=x\"%08x\"\n", reg_name, address);
                fprintf(fp, "                             and mm_pb_i.addr<x\"%08x\") else -- %s\n", address + size, reg_name);
            }
            address += size * 4; // incr address [expr $size*4]
        }
    }
    fputs("    '1';\n\n", fp);

    /*
     * Generate the error status
     */
    address = 0;
    fputs("\n  mm_pb_o.err  <=\n", fp);
    i = paramIndex + 2; // refers to the first token (key) within the value bundle of found key "parameters"
    j = 0;
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        int width = 0;
        int size = 1;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            j = i + 2;
            for (;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get value of key "mode"
                }
                else if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &size); // Get value of key "size"
                } 
                j += 2;
            } // end-for
            i = j;
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (strcmp(mode_str, "ro") == 0 || strcmp(mode_str, "wo") == 0 || strcmp(mode_str, "rw") == 0) {
                fprintf(fp, "    '0' when mm_pb_i.addr=x\"%08x\" else -- %s\n", address, reg_name);
            }
            if (strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "    PAR_i.%s_error  when mm_pb_i.addr=x\"%08x\" else -- %s\n", reg_name, address, reg_name);
            }
            if (strcmp(mode_str, "mm") == 0) {
                //set size [dict get $value size]
                fprintf(fp, "    '0' when mm_pb_i.addr>=x\"%08x\" and mm_pb_i.addr<x\"%08x\" else -- %s\n", address, address + size, reg_name);
            }
            address += size * 4; // incr address [expr $size*4]
        }
    }
    fputs("    '1';\n\n", fp);

    fputs("end rtl;\n\n", fp);
    fclose(fp);

    printf(" Done (%d).\n", ++iProcessCounter);
}

/*
 *
 */
JSONVHDL_API void Generate_Vhdl_Parset (const char * json, jsmntok_t * tokens, size_t num_tok, char * fileLocation) {
    // Index i=0 is for the token object that holds the whole json content. Hence, i=0 should be skipped.
    // Index i=1 is for token containing the vhdl (module) file name.    

    char module_name[LEN_FILENAME];
    char submodule_name[LEN_FILENAME];
    char json_filename[LEN_FILENAME];
    char tok_str[LEN_STR];
    char * json_filepath = NULL;
    //char * pTemp = NULL;
    int iLenFilepath = 0;

    module_name[0] = '\0'; // Make string empty
    submodule_name[0] = '\0';
    json_filename[0] = '\0';
    tok_str[0] = '\0';

    if (!Get_TokenString(json, tokens, num_tok, 1, module_name)) {
        fprintf(stderr, "Error (Generate_Vhdl): There is no module name given. Please check json file.\n");
        exit(EXIT_FAILURE);
    }
    
    /*
     * Implementation of dict_get_pb_files
     */
    int ArrSize = 0; // Array that contains number of tokens that contains value for key "file".
    int iIndex = 0;
    int * ArrFiles = NULL; // Array that contains number of tokens that contains value for key "file".

    // Determines number of all tokens with key "file".
    for (int i = 0; i < num_tok; i++) {
        //strncpy(tok_str, "", LEN_STR); // memcpy(tok_str, "", LEN_STR); // clear string
        tok_str[0] = '\0'; // Make string empty
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue; // Copy string of tokens[index] to [tok_str]
        if (strcmp(tok_str, "file") != 0) continue; // Compare if string [tok_str] is equal "file" => If not, next loop.        
        ArrSize += 1;
    }

    //int * ArrFiles = (int *) malloc(ArrSize * sizeof(int)); // 1 int = 4 byte. This array contains indices of tokens
    ArrFiles = (int *) calloc(ArrSize, sizeof(int)); // Allocate memory and initialize all bytes with 0
    memset(ArrFiles, 0, ArrSize * sizeof(int)); // Fill all array elements (= all bytes of array) with value 0

    // Retrieve the tokens that contain the key "file". Then remember the index (i+1) of next token that contains value of key "file".
    for (int i = 0; i < num_tok; i++) {
        //strncpy(tok_str, "", LEN_STR); // memcpy(tok_str, "", LEN_STR); // clear string      
        tok_str[0] = '\0';
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue; // Copy string of tokens[index] to [tok_str]
        if (strcmp(tok_str, "file") != 0) continue; // Compare if string [tok_str] is equal "file" => If not, next loop.
        if (iIndex >= ArrSize) {
            fprintf(stderr, "Error (Generate_Vhdl_Parset): Array index out ouf bound. Please increase size of array ArrFiles.\n");
            exit(EXIT_FAILURE);
        }
        ArrFiles[iIndex] = i+1; // If token (index i) with string "file" is found. Remember index (i+1) of its next sibling.
        ++iIndex;
    }

    // Attempt to eliminate duplicate(s)
    for (int i = 0; i < ArrSize-1; i++) {
        if (!ArrFiles[i]) continue;
        for (int j = i+1; j < ArrSize; j++) {
            if (!ArrFiles[j]) continue;
            if (Compare_TokenStrings(json, tokens, num_tok, ArrFiles[i], ArrFiles[j])) {
                ArrFiles[j] = 0; // If content of tokens[i] and tokens[j] are the same, set j to zero (0)
            }
        }
    }

    // Iterate array ArrFiles
    for (int i = 0; i < ArrSize; i++) {
        if (!ArrFiles[i]) continue;
        //strncpy(tok_str, "", strlen(tok_str)); // memcpy(tok_str, "", LEN_STR); // clear string
        tok_str[0] = '\0'; // Make string empty
        Get_TokenString(json, tokens, num_tok, ArrFiles[i], tok_str); // Get content of token at index ArrFiles[i] and store in [tok_str]
        Replace_Char_In_String(tok_str, '\\', '/'); // Replace escape character '\' by '/'
        Get_FileName(tok_str, json_filename, LEN_FILENAME); // [tok_str] contains the string for key "file" (i.e. filepath). The filepath is then stored in [json_filename].
        Get_FileRootName(tok_str, submodule_name, LEN_FILENAME);
        //==============================================================================================
        iLenFilepath = strlen(fileLocation) + strlen(tok_str) + 1; // including null-terminated '\0'
        json_filepath = (char *) malloc(iLenFilepath);
        //strncpy(json_filepath, "", iLenFilepath); // Make string empty
        json_filepath[0] = '\0'; // Make string empty
        Configure_FilePath(fileLocation, tok_str, json_filepath, iLenFilepath);
        //==============================================================================================
        //Generate_Parset(json_filename, module_name, submodule_name);
        Generate_Parset(json_filepath, module_name, submodule_name);
        free(json_filepath);
        json_filepath = NULL;
        //strncpy(json_filename, "", strlen(json_filename)); // memcpy(json_filename, "", LEN_FILENAME);
        //strncpy(submodule_name, "", strlen(submodule_name)); // memcpy(submodule_name, "", LEN_FILENAME);
        json_filename[0] = '\0'; // clear string
        submodule_name[0] = '\0';
    }

    free(ArrFiles);
}

/*
 *
 */
JSMN_API void Generate_Axi_Parbank (parbank_list_t * pbList, char * module_name) {
    FILE * fp = NULL;
    parbank_data_t * pData = NULL;

    /* Create name for *.vhd file and title for header */
    int len = 8 + strlen(module_name) + 1; // parbank_<module_name> PLUS null-terminated '\0'
    char * parbank_name = (char *) malloc(len); // => parbank_<module_name>
    char * entity_name = (char *) malloc(4 + len); // => axi_parbank_<module_name>
    char * vhd_filename = (char *) malloc(4 + len + 4); // 4 characters for file extension ".vhd" => axi_parbank_<module_name>.vhd
    char * header_title = (char *) malloc(len + 4); // => parbank_<module_name>_pkg
    strncpy(parbank_name, "", len); // Initialize empty string. Otherwise, some unwanted nonprintable characters (e.g. \220...) may appear
    strncpy(entity_name, "", 4 + len);
    strncpy(vhd_filename, "", 4 + len + 4);
    strncpy(header_title, "", len + 4);
    Str_Concat(parbank_name, len, 2, "parbank_", module_name);
    Str_Concat(entity_name, 4 + len, 2, "axi_", parbank_name);
    Str_Concat(vhd_filename, 4 + len + 4, 2, entity_name, ".vhd");
    Str_Concat(header_title, len + 4, 2, parbank_name, "_pkg");

    fp = fopen(vhd_filename, "w");
    printf("Generate_Axi_Parbank...\nGenerate \"%s.vhd\" =>", entity_name);

    /* Generate the header */
    Generate_Vhdl_Header(fp, header_title);

    fputs("use work.pbgen_pkg.all;\n", fp);
    fprintf(fp, "use work.parbank_%s_pkg.all;\n", module_name);
    fputs("use work.axi_pkg.all;\n\n", fp);
    fprintf(fp, "entity %s is\n", entity_name);
    fputs("  port (\n\n", fp);

    fputs("  S_AXI_i : in  REC_AXI_32BIT_M2S;\n", fp);
    fputs("  S_AXI_o : out REC_AXI_32BIT_S2M;\n\n", fp);
    fputs("  PAR_i : in  REC_PAR_PB_IN;\n", fp);
    fputs("  PAR_o : out REC_PAR_PB_OUT\n\n", fp);
    fputs("  );\n", fp);
    fprintf(fp, "end entity %s;\n\n", entity_name);
    fprintf(fp, "architecture rtl of %s is\n\n", entity_name);
    fputs("component pbgen_axi_slave is\n", fp);
    fputs("generic (\n", fp);
    fputs("  TIMEOUT_LIMIT_G : natural := 100\n", fp);
    fputs("  );\n", fp);
    fputs("port (\n", fp);
    fputs("  mm_pb_o : out REC_MM_PB_MOSI;\n", fp);
    fputs("  mm_pb_i : in  REC_MM_PB_MISO;\n\n", fp);
    fputs("  S_AXI_i : in  REC_AXI_32BIT_M2S;\n", fp);
    fputs("  S_AXI_o : out REC_AXI_32BIT_S2M\n", fp);
    fputs("  );\n\n", fp);
    fputs("end component pbgen_axi_slave;\n\n", fp);
    fprintf(fp, "component %s is\n", parbank_name);
    fputs("port (\n", fp);

    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        char submodule_name[LEN_FILENAME] = "";
        Get_FileRootName(pData->file, submodule_name, LEN_FILENAME);
        fprintf(fp, "  par_%s_i : in  REC_PARSET_%s_IN;\n", pData->name, submodule_name);
        fprintf(fp, "  par_%s_o : out REC_PARSET_%s_OUT;\n\n", pData->name, submodule_name);
        pData = pData->next;
    }

    fputs("  mm_pb_i : in  REC_MM_PB_MOSI;\n", fp);
    fputs("  mm_pb_o : out REC_MM_PB_MISO \n", fp);
    fputs("  );\n\n", fp);
    fprintf(fp, "end component %s;\n\n" , parbank_name);
    fputs("signal mm_pb_mosi_s : REC_MM_PB_MOSI;\n", fp);
    fputs("signal mm_pb_miso_s : REC_MM_PB_MISO;\n\n" , fp); 
    fputs("begin\n\n", fp);
    fputs("pbgen_axi_slave_i : pbgen_axi_slave\n", fp);
    fputs("generic map (\n", fp);
    fputs("  TIMEOUT_LIMIT_G => 100\n", fp);
    fputs(")\n", fp);
    fputs("port map (\n", fp);
    fputs("  mm_pb_o => mm_pb_mosi_s,\n", fp);
    fputs("  mm_pb_i => mm_pb_miso_s,\n", fp);
    fputs("  S_AXI_i => S_AXI_i,\n", fp);
    fputs("  S_AXI_o => S_AXI_o\n", fp);
    fputs(");\n\n", fp);
    fprintf(fp, "%s_i : %s\n", parbank_name, parbank_name);
    fputs("port map (\n", fp);

    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        char submodule_name[LEN_FILENAME] = "";
        Get_FileRootName(pData->file, submodule_name, LEN_FILENAME);
        fprintf(fp, "  par_%s_i => PAR_i.%s,\n", pData->name, pData->name);
        fprintf(fp, "  par_%s_o => PAR_o.%s,\n\n", pData->name, pData->name);
        pData = pData->next;
    }

    fputs("  mm_pb_o => mm_pb_miso_s,\n", fp);
    fputs("  mm_pb_i => mm_pb_mosi_s\n", fp);
    fputs(");\n\n", fp);
    fputs("end rtl;\n\n\n", fp);    

    /* Release allocated memory */
    free(header_title);
    free(vhd_filename);
    free(entity_name);
    free(parbank_name);

    fclose(fp);

    printf(" Done (%d).\n", ++iProcessCounter);
}

/*
 *
 */
JSMN_API void Generate_Parbank (parbank_list_t * pbList, char * module_name) {
    FILE * fp = NULL;
    parbank_data_t * pData = NULL;

    /* Create name for *.vhd file and title for header */
    int len = 8 + strlen(module_name) + 1; // parbank_<module_name> PLUS null-terminated '\0'
    char * entity_name = (char *) malloc(len); // => parbank_<module_name>
    char * vhd_filename = (char *) malloc(len + 4); // 4 characters for file extension ".vhd" => parbank_<module_name>.vhd
    char * header_title = (char *) malloc(8 + len + 4); // => parbank_parbank_<module_name>_pkg
    strncpy(entity_name, "", len); // Initialize empty string. Otherwise, some unwanted nonprintable characters (e.g. \220...) may appear
    strncpy(vhd_filename, "", len + 4);
    strncpy(header_title, "", 8 + len + 4);
    Str_Concat(entity_name, len, 2, "parbank_", module_name);
    Str_Concat(vhd_filename, len + 4, 2, entity_name, ".vhd");
    Str_Concat(header_title, 8 + len + 4, 3, "parbank_", entity_name, "_pkg");

    fp = fopen(vhd_filename, "w");
    printf("Generate_Parbank...\nGenerate \"%s.vhd\" =>", entity_name);

    /* Generate the header */
    Generate_Vhdl_Header(fp, header_title);

    fputs("use work.pbgen_pkg.all;\n", fp);
    fprintf(fp, "use work.parbank_%s_pkg.all;\n", module_name);
    fprintf(fp, "entity %s is\n", entity_name);
    fputs("  port (\n", fp);

    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        char submodule_name[LEN_FILENAME] = "";
        Get_FileRootName(pData->file, submodule_name, LEN_FILENAME);
        fprintf(fp, "    par_%s_i : in  REC_PARSET_%s_IN;\n", pData->name, submodule_name);
        fprintf(fp, "    par_%s_o : out REC_PARSET_%s_OUT;\n\n", pData->name, submodule_name);
        pData = pData->next;
    }
    fputs("    mm_pb_i      : in  REC_MM_PB_MOSI; -- memory mapped record for master out, slave in\n", fp);
    fputs("    mm_pb_o      : out REC_MM_PB_MISO  -- memory mapped record for master in, slave out\n\n", fp);
    fputs("  );\n", fp);
    fprintf(fp, "end entity %s;\n\n", entity_name);
    fprintf(fp, "architecture rtl of %s is\n\n", entity_name);

    /* Generate the signals */
    pData = pbList->begin;
    while (pData != NULL) {
        fprintf(fp, "  signal mm_pb_%s_i_s : REC_MM_PB_MOSI;\n", pData->name);
        fprintf(fp, "  signal mm_pb_%s_o_s : REC_MM_PB_MISO;\n\n", pData->name);
        pData = pData->next;
    }
    fputs("begin\n\n", fp);

    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        char submodule_name[LEN_FILENAME] = "";
        Get_FileRootName(pData->file, submodule_name, LEN_FILENAME);
        int addr_vec_left = (int)(round(ceil(log(pData->size)/log(2))) - 1);
        fprintf(fp, "mm_pb_%s_i_s.clk  <= mm_pb_i.clk ;\n", pData->name);
        fprintf(fp, "mm_pb_%s_i_s.resn <= mm_pb_i.resn;\n", pData->name);
        fprintf(fp, "mm_pb_%s_i_s.addr(31 downto %d+1) <= (others=>'0');\n", pData->name, addr_vec_left);
        fprintf(fp, "mm_pb_%s_i_s.addr(%d downto 0) <= mm_pb_i.addr(%d downto 0);\n", pData->name, addr_vec_left, addr_vec_left);
        fprintf(fp, "mm_pb_%s_i_s.data <= mm_pb_i.data;\n", pData->name);
        fprintf(fp, "mm_pb_%s_i_s.wren <= mm_pb_i.wren;\n", pData->name);
        fprintf(fp, "mm_pb_%s_i_s.rden <= mm_pb_i.rden;\n", pData->name);
        fprintf(fp, "mm_pb_%s_i_s.csel <= '1' when mm_pb_i.addr >= x\"%08x\" and mm_pb_i.addr < x\"%08x\" else '0';\n\n", pData->name, pData->offset, pData->offset + pData->size);
        fprintf(fp, "parset_%s : entity work.parset_%s\n", pData->name, submodule_name);
        fputs("  port map (\n", fp);
        fprintf(fp, "      mm_pb_i      => mm_pb_%s_i_s,\n", pData->name);
        fprintf(fp, "      mm_pb_o      => mm_pb_%s_o_s,\n\n", pData->name);
        fprintf(fp, "      PAR_i        => par_%s_i,\n", pData->name);
        fprintf(fp, "      PAR_o        => par_%s_o\n", pData->name);
        fputs("  );\n\n", fp);
        pData = pData->next;
    }

    /* MUX the output data */
    fputs("mm_pb_o.data <=\n", fp);
    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        fprintf(fp, "    mm_pb_%s_o_s.data when mm_pb_i.addr >= x\"%08x\" and mm_pb_i.addr < x\"%08x\" else -- offset=%u, size=%u\n", pData->name, pData->offset, pData->offset + pData->size, pData->offset, pData->size);
        pData = pData->next;
    }  
    fputs("x\"00000000\";\n\n", fp);

    /* MUX the ready signals */
    fputs("mm_pb_o.rdy  <=\n", fp);
    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        fprintf(fp, "    mm_pb_%s_o_s.rdy  when mm_pb_i.addr >= x\"%08x\" and mm_pb_i.addr < x\"%08x\" else -- %u\n", pData->name, pData->offset, pData->offset + pData->size, pData->size);
        pData = pData->next;
    }
    fputs("           '1';\n\n", fp);

    /* MUX the error signals */
    fputs("mm_pb_o.err  <=\n", fp);
    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        fprintf(fp, "    mm_pb_%s_o_s.err  when mm_pb_i.addr >= x\"%08x\" and mm_pb_i.addr < x\"%08x\" else -- %u\n", pData->name, pData->offset, pData->offset + pData->size, pData->size);
        pData = pData->next;
    }
    fputs("        '1';\n\n", fp);    
    fputs("end rtl;\n\n", fp);

    /* Release allocated memory */
    free(header_title);
    free(vhd_filename);
    free(entity_name);

    fclose(fp);

    printf(" Done (%d).\n", ++iProcessCounter);
}

/*
 * Calculate the [length] and [offest] values as in the Tcl function "proc dict_get_param {input_json_dict} ".
 */
void Calculate_Parbank_Length (const char * filepath, int * dst_length) {
    char json[MAX_STR] = "";
    jsmntok_t tokens[MAX_TOK];
    int num_tok = New_JsonParseFromFile(filepath, json, tokens);

    int i = 1, j = 0;
    int paramIndex = 0;
    int iLength = 0, iSize = 0;
    for (; i < num_tok; i++) { // Retrieves for token[i] (value) of the key "parameters"
        char tok_str[LEN_STR] = "";
        //memcpy(tok_str, "", LEN_STR);
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue;
        if (strcmp(tok_str, "parameters") != 0) continue; // [tok_str] ungleich "parameters"
        paramIndex = i; // Remember index i of found key (token) "parameters"
        break; // if index of token[i] (value) for the key "parameters" is found, break loop. Remember index i
    }
    if (paramIndex == 0) {
        fprintf(stderr, "Error (Generate_Vhdl_Parset): There is no key \"parameters\" found in TemplateNG_ json file.\n");
        return;
    }
    i = paramIndex + 2; // refers to the first token (key) within the value bundle of found key "parameters"
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            iSize = 1;
            for (j = i + 2;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &iSize); // Get value of key "size"
                }
                j += 2;
            } // end-for
            iLength += (iSize * 4);
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            i = j;
        }
    } // end-for

    *dst_length = iLength;
}

/*
 *
 */
JSONVHDL_API void Generate_Vhdl_Parbank (const char * json, jsmntok_t * tokens, size_t num_tok, char * fileLocation) {    
    int i = 1; // We begin with second token (i.e. index 1). Since first token (index 0) refers to the whole json file.
    int len = tokens[i].end - tokens[i].start + 1; // strlen plus '\0'
    char * module_name = (char *) malloc(len); // module_name, e.g. "AXIL_HWIO"
    if (!Get_TokenString(json, tokens, num_tok, i, module_name)) { // Get string from tokens[1] to [module_name]
        fprintf(stderr, "Error (Generate_Vhdl_Parbank): Module name cannot be found.\n");
        exit(EXIT_FAILURE);
    }

    int j = 0;
    int iOffset = 0;
    char * subname_str = NULL;
    char * name_str = NULL;
    char * path_str = NULL;
    char * json_filepath = NULL;
    int iLenFilepath = 0; // strlen for filepath

    /* Create a list for parbank_data */
    parbank_list_t * pbList = Malloc_Parbank_List();

    /*
     * Implementation of Tcl code lines:
     *      set dic $input_json_dict
     *      set l_dic [dict_get_param $dic]
     *
     * The object $l_dic and all of its data are implemented as/in a linked list (parbank_list_t).
     */
    i += 2; // jumps over next two tokens and now refers to the first subname, e.g. "common", "chan" ...
    for (; i < num_tok;) { // i (= 1 + 2 = 3) is first line (token) named "common". New line
        int iLength = 0;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            subname_str = (char *) malloc(tokens[i].end - tokens[i].start + 1); // also including '\0'
            Get_TokenString(json, tokens, num_tok, i, subname_str); // Get subname_str, e.g. "common", "chan", ... in "AXIL_HWIO.json"
            len = strlen(module_name) + strlen(subname_str) + 2; // +1 (= '\0')  +1 (= underscore/space), e.g. ("AXIL_HWIO" + '_' + "common")  OR  ("AXIL_HWIO" + ' ' + "common")
            name_str = (char *) malloc(len); 
            path_str = (char *) malloc(len);
            strncpy(name_str, "", len-1); // Important! In order to overwrite/eliminate nonprintable characters
            strncpy(path_str, "", len-1);
            Str_Concat(name_str, len, 3, module_name, "_", subname_str); // Create Key "name" with Value e.g. "AXIL_HWIO_common"
            Str_Concat(path_str, len, 3, module_name, " ", subname_str); // Create Key "name" with Value e.g. "AXIL_HWIO common"
            int log2_size = 0;
            for (j = i + 2;;) { // Iterate through {key value} pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                if (tokens[j].type != JSMN_STRING) {
                    fprintf(stderr, "Error (Generate_Vhdl_Parbank): Invalid JSON structure. Token cannot be processed correctly.\n");
                    exit(EXIT_FAILURE);
                }
                char * tok_str = (char *) malloc(tokens[j].end - tokens[j].start + 1); // strlen plus '\0'
                char * filepath = NULL; // contains the file path, e.g. "./TemplateNG_common.json"
                if (!Get_TokenString(json, tokens, num_tok, j, tok_str)) { // Get string from tokens[j] and store to [tok_str]
                    fprintf(stderr, "Error (Generate_Vhdl_Parbank): Invalid JSON structure. Token cannot be processed correctly.\n");
                    exit(EXIT_FAILURE);
                }
                if (strcmp(tok_str, "file") == 0) {
                    filepath = (char *) malloc(tokens[j+1].end - tokens[j+1].start + 1); // strlen plus '\0'
                    strncpy(filepath, "", tokens[j+1].end - tokens[j+1].start); // Make string empty
                    Get_TokenString(json, tokens, num_tok, j+1, filepath); // Get file path name, e.g. "./TemplateNG_common.json"
                    Replace_Char_In_String(filepath, '\\', '/'); // Replace escape character '\' by '/'
                    //==============================================================================================
                    iLenFilepath = strlen(fileLocation) + strlen(filepath) + 1; // including null-terminated '\0'
                    json_filepath = (char *) malloc(iLenFilepath);
                    strncpy(json_filepath, "", iLenFilepath); // Make string empty
                    Configure_FilePath(fileLocation, filepath, json_filepath, iLenFilepath);
                    //==============================================================================================
                    // Calculate length and offset; create new list element for storing data
                    Calculate_Parbank_Length (json_filepath, &iLength);
                    //set log2_size [expr 1<<round(ceil([ld $length]))]
                    log2_size = 1 << ((int)round(ceil(log(iLength)/log(2))));
                    if ( ((log2_size-1) & iOffset) != 0 ) { // check if address is already aligned to the block size
                        // set offset [expr ($offset & (0xffffffff * $log2_size))+$log2_size]
                        iOffset = (iOffset & (0xffffffff * log2_size)) + log2_size;
                    }
                    AddDataBack_Parbank_List(pbList, name_str, path_str, iOffset, filepath, iLength);
                    free(json_filepath);
                    free(filepath);
                }
                free(tok_str);
                j += 2;
            } // end-for
            free(subname_str);
            free(name_str);
            free(path_str);
        }
        iOffset += iLength;
        i = j; // Next line
    } // end-for

    /*
     * The object $l_dic and its data are stored as/in a linked list (parbank_data_t)
     */
    parbank_data_t * pData = pbList->begin;
    while (pData != NULL) {
        printf("{name %s path {%s} Offset %d file %s size %d\n", pData->name, pData->path, pData->offset, pData->file, pData->size);
        pData = pData->next;
    }

    /* Write data in file "parbank_<module_name>.vhd" */
    Generate_Parbank(pbList, module_name);

    /* Write data in file "axi_parbank_<module_name>.vhd" */
    Generate_Axi_Parbank(pbList, module_name);

    free(module_name); // AXIL_HWIO

    /* Remove created parbank list */
    Free_Parbank_List(&pbList);
}

/*
 *
 */
JSMN_API void Generate_Parbank_Records (FILE * fp, parbank_list_t * pbList) {
    parbank_data_t * pData = NULL;

    fputs("\ntype REC_PAR_PB_OUT is\n", fp);
    fputs("  record\n", fp);

    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        char typename[LEN_STR] = "";
        Get_FileRootName(pData->file, typename, LEN_STR);
        fprintf(fp, "     %s : REC_PARSET_%s_OUT;\n", pData->name, typename);
        pData = pData->next;
    }

    fputs("  end record;\n\n", fp);
    fputs("type REC_PAR_PB_IN is\n", fp);
    fputs("  record\n", fp);

    pData = pbList->begin; // Iterates through list elements
    while (pData != NULL) {
        char typename[LEN_STR] = "";
        Get_FileRootName(pData->file, typename, LEN_STR);
        fprintf(fp, "     %s  : REC_PARSET_%s_IN;\n", pData->name, typename);
        pData = pData->next;
    }

    fputs("  end record;\n\n", fp);
}

/*
 *
 */
JSONVHDL_API void Generate_Parset_Records (FILE * fp, const char * json_filename, const char * submodule_name) {
    fprintf(fp, "\ntype REC_PARSET_%s_OUT is\n", submodule_name);
    fputs("  record\n", fp); 
    fputs("    debug   : std_logic;\n", fp);

    char json[MAX_STR] = "";
    jsmntok_t tokens[MAX_TOK];
    int num_tok = New_JsonParseFromFile(json_filename, json, tokens);

    int i = 1, j = 0;
    int paramIndex = 0;
    for (; i < num_tok; i++) { // Retrieves for token[i] (value) of the key "parameters"
        char tok_str[LEN_STR] = "";
        //memcpy(tok_str, "", LEN_STR);
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue;
        if (strcmp(tok_str, "parameters") != 0) continue; // [tok_str] not equals "parameters"
        paramIndex = i; // Remember index i of found key (token) "parameters"
        break; // if index of token[i] (value) for the key "parameters" is found, break loop. Remember index i
    }
    if (paramIndex == 0) {
        fprintf(stderr, "Error (Generate_Vhdl_Parset): There is no key \"parameters\" found in TemplateNG_ json file.\n");
        return;
    }
    i = paramIndex + 2; // refers to the first token (key) within the value-bundle of found key "parameters"
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        char sv_str[LEN_STR] = "";
        int width = 0;
        int size = 0;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            for (j = i + 2;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get string value of key "mode"
                }
                else if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &size); // Get value of key "size"
                }
                j += 2;
            } // end-for
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (width == 1) {
                Str_Concat(sv_str, LEN_STR, 1, "std_logic"); // set sv "std_logic"
            }
            else {
                sprintf(sv_str, "std_logic_vector(%d downto 0)", width - 1); // set sv "std_logic_vector([expr $width-1] downto 0)"
            }
            if (strcmp(mode_str, "wo") == 0 || strcmp(mode_str, "rw") == 0) {
                fprintf(fp, "    %s : %s;\n", reg_name, sv_str);
            }
            if (strcmp(mode_str, "wog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "    %s : %s;\n", reg_name, sv_str);
                fprintf(fp, "    %s_wr : std_logic;\n", reg_name);
            }
            if (strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "    %s_rd : std_logic;\n", reg_name);
            }
            if (strcmp(mode_str, "mm") == 0) {
                fprintf(fp, "    %s      : std_logic_vector(%d downto 0);\n", reg_name, width-1);
                fprintf(fp, "    %s_wr   : std_logic;\n", reg_name);
                fprintf(fp, "    %s_rd   : std_logic;\n", reg_name);
                if (size > 1) {
                    fprintf(fp, "    %s_addr : std_logic_vector(%d downto 0);\n", reg_name, ((int)(log(size)/log(2)))-1);
                }
            }
            i = j;
        }
    } // end-for. New line

    fputs("  end record;\n\n", fp);
    fprintf(fp, "type REC_PARSET_%s_IN is\n", submodule_name);
    fputs("  record\n", fp);
    fputs("    parameter_reset   : std_logic;\n", fp);

    j = 0;
    i = paramIndex + 2; // refers to the first token (key) within the value-bundle of found key "parameters"
    for (; i < (num_tok-1); ) {// Iterates through every token (line) as "A_type_rw", "B_type_ro", ...
        char reg_name[LEN_STR] = ""; // Reg_name, i.e. "A_type_rw"
        char tok_str[LEN_STR] = "";
        char mode_str[LEN_STR] = "";
        char sv_str[LEN_STR] = "";
        int width = 0;
        int size = 0;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            Get_TokenString(json, tokens, num_tok, i, reg_name); // Get reg_name, i.e. "A_type_rw"
            for (j = i + 2;;) { // Iterate through all pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                Get_TokenString(json, tokens, num_tok, j, tok_str); // Iterate through every token in {"width": 16,  "type" : "reg", "mode" :  "rw",  "reset" :    0, "size" : 0 },
                if (strcmp(tok_str, "mode") == 0) {
                    Get_TokenString(json, tokens, num_tok, j+1, mode_str); // Get string value of key "mode"
                }
                else if (strcmp(tok_str, "width") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &width); // Get value of key "width"
                }
                else if (strcmp(tok_str, "size") == 0) {
                    Get_TokenPrimitiveNumber(json, tokens, num_tok, j+1, &size); // Get value of key "size"
                }
                j += 2;
            } // end-for
            //printf("%s: { width: %d, mode: %s }\n", reg_name, width, mode_str);
            if (width == 1) {
                Str_Concat(sv_str, LEN_STR, 1, "std_logic"); // set sv "std_logic"
            }
            else {
                sprintf(sv_str, "std_logic_vector(%d downto 0)", width - 1); // set sv "std_logic_vector([expr $width-1] downto 0)"
            }
            if (strcmp(mode_str, "ro") == 0) {
                fprintf(fp, "    %s : %s; -- mode=%s\n", reg_name, sv_str, mode_str);
            }
            if (strcmp(mode_str, "rog") == 0 || strcmp(mode_str, "rwg") == 0) {
                fprintf(fp, "    %s : %s;\n", reg_name, sv_str);
                fprintf(fp, "    %s_busy  : std_logic;\n", reg_name);
                fprintf(fp, "    %s_error : std_logic;\n", reg_name);
            }
            if (strcmp(mode_str, "mm") == 0) {
                fprintf(fp, "    %s : std_logic_vector(%d downto 0);\n", reg_name, width-1);
                fprintf(fp, "    %s_busy  : std_logic;\n", reg_name);
            }
            i = j;
        }
    } // end-for. New line

    fputs("  end record;\n\n", fp);
}

/*
 *
 */
JSONVHDL_API void Generate_Vhdl_Records (const char * json, jsmntok_t * tokens, size_t num_tok, char * fileLocation) {    
    int i = 1; // We begin with second token (i.e. index 1). Since first token (index 0) refers to the whole json file.
    int len = tokens[i].end - tokens[i].start + 1; // strlen plus '\0'
    char * module_name = (char *) malloc(len); // module_name, e.g. "AXIL_HWIO"
    if (!Get_TokenString(json, tokens, num_tok, i, module_name)) { // Get string from tokens[1] to [module_name]
        fprintf(stderr, "Error (Generate_Vhdl_Records): Module name cannot be found.\n");
        exit(EXIT_FAILURE);
    }

    FILE * fp = NULL;
    int j = 0;
    int iOffset = 0;
    char * subname_str = NULL;
    char * name_str = NULL;
    char * path_str = NULL;
    char submodule_name[LEN_FILENAME] = "";
    char * json_filepath = NULL;
    int iLenFilepath = 0;

    /* Create a list for parbank_data */
    parbank_list_t * pbList = Malloc_Parbank_List();

    /*
     * Implementation of Tcl code lines:
     *      set dic $input_json_dict
     *      set l_dic [dict_get_param $dic]
     *
     * The object $l_dic and all of its data are implemented as/in a linked list (parbank_list_t).
     */
    i += 2; // jumps over next two tokens and now refers to the first subname, e.g. "common", "chan" ...
    for (; i < num_tok;) { // i (= 1 + 2 = 3) is first line (token) named "common". New line
        int iLength = 0;
        if (tokens[i].type == JSMN_STRING && tokens[i+1].type == JSMN_OBJECT) {
            subname_str = (char *) malloc(tokens[i].end - tokens[i].start + 1); // also including '\0'
            Get_TokenString(json, tokens, num_tok, i, subname_str); // Get subname_str, e.g. "common", "chan", ... in "AXIL_HWIO.json"
            len = strlen(module_name) + strlen(subname_str) + 2; // +1 (= '\0')  +1 (= underscore/space), e.g. ("AXIL_HWIO" + '_' + "common")  OR  ("AXIL_HWIO" + ' ' + "common")
            name_str = (char *) malloc(len); 
            path_str = (char *) malloc(len);
            strncpy(name_str, "", len); // Important! In order to overwrite/eliminate nonprintable characters
            strncpy(path_str, "", len);
            Str_Concat(name_str, len, 3, module_name, "_", subname_str); // Create Key "name" with Value e.g. "AXIL_HWIO_common"
            Str_Concat(path_str, len, 3, module_name, " ", subname_str); // Create Key "name" with Value e.g. "AXIL_HWIO common"
            int log2_size = 0;
            for (j = i + 2;;) { // Iterate through {key value} pairs in current line
                if (j >= num_tok || tokens[j+1].type == JSMN_OBJECT) break;
                if (tokens[j].type != JSMN_STRING) {
                    fprintf(stderr, "Error (Generate_Vhdl_Parbank): Invalid JSON structure. Token cannot be processed correctly.\n");
                    exit(EXIT_FAILURE);
                }
                char * tok_str = (char *) malloc(tokens[j].end - tokens[j].start + 1); // strlen plus '\0'
                char * filepath = NULL; // contains the file path, e.g. "./TemplateNG_common.json"
                if (!Get_TokenString(json, tokens, num_tok, j, tok_str)) { // Get string from tokens[j] and store to [tok_str]
                    fprintf(stderr, "Error (Generate_Vhdl_Parbank): Invalid JSON structure. Token cannot be processed correctly.\n");
                    exit(EXIT_FAILURE);
                }
                if (strcmp(tok_str, "file") == 0) {
                    filepath = (char *) malloc(tokens[j+1].end - tokens[j+1].start + 1); // strlen plus '\0'
                    Get_TokenString(json, tokens, num_tok, j+1, filepath); // Get file path name, e.g. "./TemplateNG_common.json"
                    Replace_Char_In_String(filepath, '\\', '/'); // Replace escape character '\' by '/'
                    //==============================================================================================
                    iLenFilepath = strlen(fileLocation) + strlen(filepath) + 1; // including null-terminated '\0'
                    json_filepath = (char *) malloc(iLenFilepath);
                    //strncpy(json_filepath, "", iLenFilepath); // Make string empty
                    json_filepath[0] = '\0'; // Make string empty
                    Configure_FilePath(fileLocation, filepath, json_filepath, iLenFilepath);
                    //==============================================================================================
                    // Calculate length and offset; create new list element for storing data
                    Calculate_Parbank_Length (json_filepath, &iLength);
                    //set log2_size [expr 1<<round(ceil([ld $length]))]
                    log2_size = 1 << ((int)round(ceil(log(iLength)/log(2))));
                    if ( ((log2_size-1) & iOffset) != 0 ) { // check if address is already aligned to the block size
                        // set offset [expr ($offset & (0xffffffff * $log2_size))+$log2_size]
                        iOffset = (iOffset & (0xffffffff * log2_size)) + log2_size;
                    }
                    AddDataBack_Parbank_List(pbList, name_str, path_str, iOffset, filepath, iLength);
                    free(filepath);
                    free(json_filepath);
                }
                free(tok_str);
                j += 2;
            } // end-for
            free(subname_str);
            free(name_str);
            free(path_str);
        }
        iOffset += iLength;
        i = j; // Next line
    } // end-for

    /*
     * The object $l_dic and its data are stored as/in a linked list (parbank_data_t)
     */
    parbank_data_t * pData = pbList->begin;
    while (pData != NULL) {
        printf("{name %s path {%s} Offset %d file %s size %d\n", pData->name, pData->path, pData->offset, pData->file, pData->size);
        pData = pData->next;
    }

    /*
     * Write records
     */
    // Create name for *.vhd file and title for header
    len = 8 + strlen(module_name) + 4 + 1; // parbank_<module_name>_pkg PLUS null-terminated '\0'
    char * entity_name = (char *) malloc(len); // => parbank_<module_name>_pkg
    char * vhd_filename = (char *) malloc(len + 4); // 4 characters for file extension ".vhd" => parbank_<module_name>_pkg.vhd
    strncpy(entity_name, "", len); // Initialize empty string. Otherwise, some unwanted nonprintable characters (e.g. \220...) may appear
    strncpy(vhd_filename, "", len + 4);
    Str_Concat(entity_name, len, 3, "parbank_", module_name, "_pkg");
    Str_Concat(vhd_filename, len + 4, 2, entity_name, ".vhd");

    fp = fopen(vhd_filename, "w");
    printf("Generate_Vhdl_Records...\nGenerate \"%s.vhd\" =>", entity_name);

    /* Generate the header */
    Generate_Vhdl_Header(fp, entity_name);
    fprintf(fp, "\npackage parbank_%s_pkg is\n", module_name);

    /*
     * Add the records from parsets (Implementation of dict_get_pb_files)
     */
    char tok_str[LEN_STR] = "";
    int ArrSize = 0;
    int iIndex = 0;
    int * ArrFiles = NULL; // Array that contains number of tokens that contains value for key "file".

    // Determines number of all tokens with key "file".
    for (int i = 0; i < num_tok; i++) {
        strncpy(tok_str, "", LEN_STR); // memcpy(tok_str, "", LEN_STR); // clear string      
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue; // Copy string of tokens[index] to [tok_str]
        if (strcmp(tok_str, "file") != 0) continue; // Compare if string [tok_str] is equal "file" => If not, next loop.        
        ArrSize += 1;
    }
    // Create array containing the indices of values for key "file".
    //int * ArrFiles = (int *) malloc(ArrSize * sizeof(int)); // 1 int = 4 byte. This array contains indices of tokens
    ArrFiles = (int *) calloc(ArrSize, sizeof(int)); // Allocate memory and initialize all bytes with 0
    memset(ArrFiles, 0, ArrSize * sizeof(int)); // Fill all array elements (= all bytes of array) with value 0

    // Retrieve the tokens that contain the key "file". Then remember the index (i+1) of next token that contains value of key "file".
    for (int i = 0; i < num_tok; i++) {
        strncpy(tok_str, "", LEN_STR); // memcpy(tok_str, "", LEN_STR); // clear string      
        if (!Get_TokenString(json, tokens, num_tok, i, tok_str)) continue; // Copy string of tokens[index] to [tok_str]
        if (strcmp(tok_str, "file") != 0) continue; // Compare if string [tok_str] is equal "file" => If not, next loop.
        if (iIndex >= ArrSize) {
            fprintf(stderr, "Error (Generate_Vhdl_Parset): Array index out ouf bound. Please increase size of array ArrFiles.\n");
            exit(EXIT_FAILURE);
        }
        ArrFiles[iIndex] = i+1; // If token (index i) with string "file" is found. Remember index (i+1) of its next sibling.
        ++iIndex;
    }

    // Attempt to eliminate duplicate(s)
    for (int i = 0; i < ArrSize-1; i++) {
        if (!ArrFiles[i]) continue;
        for (int j = i+1; j < ArrSize; j++) {
            if (!ArrFiles[j]) continue;
            if (Compare_TokenStrings(json, tokens, num_tok, ArrFiles[i], ArrFiles[j])) {
                ArrFiles[j] = 0; // If content of tokens[i] and tokens[j] are the same, set j to zero (0)
            }
        }
    }

    // Iterate array ArrFiles
    for (int i = ArrSize-1; i >= 0; i--) {
        if (!ArrFiles[i]) continue;
        strncpy(tok_str, "", strlen(tok_str)); // memcpy(tok_str, "", LEN_STR); // clear string
        Get_TokenString(json, tokens, num_tok, ArrFiles[i], tok_str); // Get content of token at index ArrFiles[i] and store in [tok_str]
        //==============================================================================================
        iLenFilepath = strlen(fileLocation) + strlen(tok_str) + 1; // including null-terminated '\0'
        json_filepath = (char *) malloc(iLenFilepath);
        //strncpy(json_filepath, "", iLenFilepath); // Make string empty
        json_filepath[0] = '\0'; // Make string empty
        Configure_FilePath(fileLocation, tok_str, json_filepath, iLenFilepath);
        //==============================================================================================
        Get_FileRootName(tok_str, submodule_name, LEN_FILENAME);
        //Generate_Parset_Records(fp, tok_str, submodule_name); // Add the records from parsets
        Generate_Parset_Records(fp, json_filepath, submodule_name); // Add the records from parsets
        free(json_filepath); json_filepath = NULL;
        //strncpy(submodule_name, "", strlen(submodule_name)); // memcpy(submodule_name, "", LEN_FILENAME);
        submodule_name[0] = '\0'; // Make empty string
    }

    /* Add the records from the parbanks */
    Generate_Parbank_Records(fp, pbList);
    fputs("\n\nend;\n\n", fp);

    /* Release allocated memory */
    fclose(fp);

    free(ArrFiles);
    free(vhd_filename);
    free(entity_name);
    free(module_name); // AXIL_HWIO

    /* Remove created parbank list */
    Free_Parbank_List(&pbList);

    printf(" Done (%d).\n", ++iProcessCounter);
}
  
/*
 *
 */
JSONVHDL_API void Start_PbGen (const char * filepath) {
    char json[MAX_STR] = ""; // stores the content of json file in [json]
    jsmn_parser parser; // Json parser
    jsmntok_t tokens[MAX_TOK];
    int r = 0; // Number of calculated jsmn tokens from read json file
    size_t iStrLen = strlen(filepath) + 1;

    /* Read file [file_json] and store its content to [json] */
    if (Read_File(filepath, json) != EXIT_SUCCESS) { exit(EXIT_FAILURE); }

    /* Replace escape character '\' by '/' */
    Replace_Char_In_String(json, '\\', '/');

    /* Initialize jsmn/json parser */
    jsmn_init(&parser);
    r = jsmn_parse(&parser, json, strlen(json), tokens, sizeof(tokens) / sizeof(tokens[0]));

    if (r <= 1) {
        fprintf(stderr, "Error (Start_PbGen): There is not enough tokens.\n");
        exit(EXIT_FAILURE);
    }

    /* File location */
    char * fileLocation = (char *) malloc(iStrLen);
    strncpy(fileLocation, "", iStrLen); // Make string empty
    if ( !Get_FileLocation(filepath, fileLocation, iStrLen) ) {
        free(fileLocation);
        fileLocation = NULL;
    }

    /* Generate vhdl code */
    Generate_Vhdl_Parset(json, tokens, r, fileLocation);
    Generate_Vhdl_Parbank(json, tokens, r, fileLocation);
    Generate_Vhdl_Records(json, tokens, r, fileLocation);

    if (fileLocation != NULL) {
        free(fileLocation);
        fileLocation = NULL;
    }
    printf("\nFinish.\n");
}

#endif /* JSONVHDL_H */