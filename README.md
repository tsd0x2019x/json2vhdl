# Json2Vhdl

# (Plain) C
Implementation using plain C WITHOUT any Tcl C-API and dependencies.

Remove old binary:

    $ make clean

Compile:

    $ make

Execute:

1. Windows:
	1.1 Run "start_pbgen.exe" followed by a json file name:
    
			$ start_pbgen.exe AXIL_HWIO.json
			
2. Linux (Ubuntu):
	2.1 Run the binary "start_pbgen" followed by a json file name:

			$ ./start_pbgen AXIL_HWIO.json


# TclC
C application with embedded Tcl (usign Tcl C-API)

Remove old binary:

    $ make clean

Compile:

    $ make

Execute:

    $ ./TclC AXIL_HWIO.json


# Visual Sutdio Code

To debug with VS Code:

1. In VS Code, click [Debug]
2. Click [File]->[Open Folder...] to open (working) folder
3. Choose and navigate in that folder (where the *.c and *.h files are in)
4. Click [Ok]
5. Add configuration files ("launch.json", "tasks.json")


# Cross-compile for Windows and Linux
1. Install Mingw-w64:

    $ sudo apt-get install mingw-w64

2. Compile in Linux using GCC:

    $ /usr/bin/gcc

 3. Cross-compile for Windows using Mingw-w64:
 
    3.1. For 32-Bit: 

        $ /usr/bin/i686-w64-mingw32-gcc <progname.c> -o <progname>

    3.2. For 64-Bit:

        $ /usr/bin/x86_64-w64-mingw32-gcc <progname.c> -o <progname>